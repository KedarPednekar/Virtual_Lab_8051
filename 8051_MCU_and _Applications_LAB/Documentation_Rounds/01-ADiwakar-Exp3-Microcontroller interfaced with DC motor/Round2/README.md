## Round 2

Experiment 3: Interfacing of 8051 Microcontroller with DC motor.

### 1. Story Outline:
<p align="justify">The experiment comprises of DC motor that has been interfaced with a 8051 Microcontroller. Now so as to see the output it is essential that a code must be written using the Assembly Language. There are 4 ports i.e. Port 0,1,2 and 3. The user can select any port as per their convenience. Once the code is written then for line by line execution a user has to simply use the Debug option for execution of an individual line. Finally the output will be visible on the peripheral that is interfaced with the microcontroller. </p>

### 2. Story:
##### 2.1 Set the visual stage description:

When the user visits the simulator section, a simulation window will be visible. An important message window will pop-up, a user must make sure that the he/she should properly understand the message before performing any task on the simulator.

The simulator window consists of pin diagram of 8051 microcontroller, memory, peripheral (DC motor) and an editor window. The 8051 has 4 ports with a LED connected across each pin of the microcontroller, the LEDs indicate if a logic "HIGH" or "LOW" is applied to a specific port's pin.

The memory consists of several registers and the locations, the values stored in them will be visible in the memory.

The peripheral interfaced with 8051 is actually a DC motor, there will be two port pins across which this DC motor will be connected. Now the user has to apply a specific logic (i.e. HIGH or LOW) across the two pins and observe whether the motor rotates or not. The motor may rotate in clockwise or anticlockwise direction depending on the input given to the pins by the user.

The editor window is on the extreme right, a user must write a code, the assembly language should be used. A sample code has been provided so as to ensure that the user will get an idea about the exact way to make the motor rotate in a specific direction. The user can write their own code.


##### 2.2 Set User Objectives & Goals:
1. The prime objective of the experiment is to demonstrate the working of DC motor.
2. To understand in which direction the motor will rotate once input is given to the two pins of the motor.
3. To understand the basics of assembly coding using the sample code provided.
4. To apply a new logic in the coding apart from the sample code, writing a code to achieve the goal (Rotating a motor or stopping its rotation).
5. To know that other port pins can be used apart from the default port pins.
6. To understand that the motor can rotate either in clockwise or anticlockwise direction. It can be stopped by applying specific input logic to the input pins.
7. To understand whether a HIGH (i.e. 1) or a LOW (i.e. 0) has to be applied to the input pins of the DC motor so as to rotate it in the desired direction. This objective can be easily achieved by reading the theory.
8. To compare the working of DC motor in the real life and on the virtual platform, analyze the difference observed and comment on the same with a fundamental reasoning.
9. Finally click on the "Print Simulation Window" so that a PDF can be generated of the simulation that a user has done on the simulator.

##### 2.3 Set the pathway activities:
1. <p align="justify">The set-up consists of a microcontroller interfaced with a peripheral. In this experiment a DC motor is connected to two port pins of 8051. If the user wants to change the port pins then he/she can do it as per his/her convenience. Initially an important message will pop-up, users must read it and understand that the simulator has some boundaries and so a user should work within these boundaries. </p>
2. <p align="justify">There is an editor visible in the simulator window, it is expected that a user must write an assembly language code so as to verify the working of the peripheral interfaced with microcontroller.  </p>
3. <p align="justify">There is a code provided as sample or as a reference for a user. This code can be copied and pasted in the editor so as to observe the working of the peripheral. Users can apply their own logic and explore different ways to achieve the common goal of observing the working of the peripheral. </p>
4. <p align="justify">There are three options provided at the bottom of the editor. The first option is Run, simply by pressing this option the entire code will be executed at once. </p>
5. <p align="justify">The second option is Debug, this option can come in handy in case if a user is interested to observe the execution of each line in the written code. </p>
6. <p align="justify">The third option is Reset, this option resets the microcontroller by resetting all the registers to their default values. </p>
7. <p align="justify">If the code written by the user has some syntax errors as per the instruction set of 8051, the simulator will highlight those lines with corresponding error. </p>
8. <p align="justify">So the user will have to rectify it with appropriate instruction and run the program again. This makes their assembly programming more clear. </p>
6. <p align="justify">After running their program, user will analyze the status of corresponding pins, register values in architecture, output rotation of DC Motor. </p>
7. <p align="justify">After performance a user can print the simulator window and take its printout by simply clicking the "Print Simulation Window" option. </p>

##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:
1. What will happen if both the input to DC motor are at logic HIGH.
2. What will happen if both the input to DC motor are at logic LOW.
3. Rotate motor in a direction after some calculation written in assembly program results in desired value.

##### 2.5 Allow pitfalls:
1. An invalid assembly language instruction in the code written by the user may cause an error. The user must have to use the valid instruction set of 8051.
2. When a user tries to use infinite loop code (i.e. HERE : JNB P0.0 HERE), which relies on external input(s) to break the loop, the site will hang, prompting you to reload the site. Thus, user can understand from the mistake that an infinite loop code must not be written, instead an alternative must be found to achieve the goal.
3. Software generated delay programs will not work in given simulator. This is because the multiple iterations of loop executing in delay-program makes the web page unresponsive. Instead an alternative must be found to achieve the goal. In such case Debug option is suggested.
4. Writing a code taking into consideration a specific port pin and not selecting the specific port pin below the peripheral in the simulator will result in unexpected output. So user must take into consideration the usage of same port pin in the peripheral section as well as while writing the code.

##### 2.6 Conclusion:
1. The user will be able to understand what input should be applied to a DC motor.
2. The rotation of a motor in any direction actually depends on the input applied to its pins.
3. Student will learn to interface and program 8051 microcontroller in assembly language without hardware setup. This would enable the students to design their own application using 8051 microcontroller and DC motor.

##### 2.7 Equations/formulas: NA

### 3. Flowchart:
<img src="flowchart/flowchart.png"/><br>
<a href="https://docs.google.com/drawings/d/1_9XbcFUK6c1fqyC2Z7Hj46QsQqqxOFcTrgBzkJoz87A/edit?usp=sharing" target="_blank"> Click for flowchart</a>
### 4. Mindmap:
<img src="mindmap/mindmap.png"/><br>
<a href="mindmap/mindmap.pdf" target="_blank"> Click for mindmap</a>

### 5. Storyboard:
<img src="Storyboard/storyboard.gif"/><br>
<a href="Storyboard/storyboard.gif" target="_blank">[here]</a>
