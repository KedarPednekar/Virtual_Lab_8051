## Round 2


Experiment 2: Interfacing of 8051 Microcontroller with ADC and DAC.

### 1. Story Outline:
<p align="justify">The experiment comprises of peripherals that are been interfaced with a 8051 Microcontroller. Here there are peripherals like ADC (Analog to Digital Converter) and DAC(Digital to Analog Converter) . Now so as to see the output of DAC it is essential that a code must be written using the Assembly Language and for observing the output of ADC the knob slider must be used as analog input. There are 4 ports i.e. Port 0,1,2 and 3. The user can select any port as per their convenience. Once the code is written then for line by line execution a user has to simply use the Debug option for execution of an individual line. Finally the output will be visible on the peripheral that is interfaced with the microcontroller. </p>

### 2. Story:
##### 2.1 Set the visual stage description:

When the user visits the simulator section, a simulation window will be visible. An important message window will pop-up, a user must make sure that the he/she should properly understand the message before performing any task on the simulator.

The simulator window consists of pin diagram of 8051 microcontroller, memory, peripheral (knob slider with analog value display) and an editor window. The 8051 has 4 ports with a LED connected on each pin of the microcontroller, the LEDs indicate if a logic "HIGH" or "LOW" is applied to a specific port's pin.

The memory consists of several registers and the locations, the values stored in them will be visible in the memory.

The peripheral interfaced with 8051 is actually a knob slider, there will be a port across which this knob slider will be connected. The knob slider actually acts like an analog value. Now this experiment comprises of DAC as well as ADC (note: The ADC and DAC is assumed to be inbuilt with the knob slider).

The editor window is on the extreme right, a user must write a code, the assembly language should be used. A sample code has been provided so as to ensure that the user will get an idea about the exact working of the DAC. The user can write their own code for DAC, this code will actually act like an input. Further for knowing ADC, the user must move the knob for providing input to the ADC.

##### 2.2 Set User Objectives & Goals:
1. The prime objective of the experiment is to demonstrate the working of DAC as well as ADC with 8051 microcontroller.
2. To understand how a DAC actually works.
3. To understand the basics of assembly programming using the sample code provided for DAC.
4. To apply a new logic in the coding apart from the sample code, writing a code to achieve the goal (shifting the position of the knob on the knob slider and to generate an analog voltage value).
5. To know that other port can be used apart from the default port.
6. To understand how a ADC works.
7. To know that in case of ADC it is essential that an analog input has to be applied, so in this case the knob slider actually acts like an analog input. Simply a user has to move the knob and then he/she can see the digital output across the LED pins connected to the port pins of the user's desire. (For e.g. If the knob is moved and the value visible is 2.84V, this value is in decimal, if converted to hexadecimal we get 91H. The binary representation of 91H will be 10010001. The binary value can be observed across the LEDs connected across the port pins.)
8. To compare the working of DAC and ADC in the real life and on the virtual platform, analyze the difference observed and comment on the same with a fundamental reasoning.
9. Attempt the assessment questions, there are 3 sections i.e. Conceptual, Analytical and Problem Solving. Each category will check the understanding of a user. For 100 % completion a user must solve all the questions by selecting each section and writing the conclusion.
10. Finally click on the "Submit Answer" so that a PDF can be generated of the user's response. The assessor then can evaluate the marks scored by the user.

##### 2.3 Set the pathway activities:
1. <p align="justify">The set-up consists of a microcontroller interfaced with peripherals. In this experiment a knob slider is connected to a port. If the user wants to change the port then he/she can do it as per his/her convenience. Initially an important message will pop up, users must read it and understand that the simulator has some boundaries and so a user should work within these boundaries. </p>
2. <p align="justify">There is an editor visible in the simulator window, it is expected that a user must write an assembly language code so as to verify the working of the peripherals interfaced with microcontroller.  </p>
3. <p align="justify">There is a code provided as sample or as a reference for a user. This code can be copied and pasted in the editor so as to observe the working of the DAC. Users can apply their own logic and explore different ways to achieve the common goal of observing the working of the peripheral. </p>
4. <p align="justify">There are three options provided at the bottom of the editor. The first option is Run, simply by pressing this option the entire code will be executed at once. </p>
5. <p align="justify">The second option is Debug, this option can come in handy in case if a user is interested to observe the execution of each line in the written code. </p>
6. <p align="justify">The third option is Reset, this option resets the microcontroller by resetting all the registers to their default values. </p>
7. <p align="justify">If the code written by the user has some syntax errors as per the instruction set of 8051, the simulator will highlight those lines with corresponding error. </p>
8. <p align="justify">So the user will have to rectify it with appropriate instruction and run the program again. This makes their assembly programming more clear. </p>
9. <p align="justify">To understand the working of DAC, an assembly program which will generate desired digital input code at pins of 8051 must be written by user in editor. This will be converted to analog output in the form of shift in slider. The user can analyze the analog output value corresponding to digital input and understand the working of DAC. </p>
10. <p align="justify">Now so as to understand the working of ADC it is but obvious that an analog input must be applied as input, the knob must be now moved. On the knob's movement there will be variation in the value (in voltage) which is displayed near the knob slider. Thus, the digital output will be visible across the LEDs connected to the port pins. </p>
11. <p align="justify">After performance a user can print the simulator window and take its printout by simply clicking the Print Simulation Window option. </p>
12. <p align="justify">There are 3 sections i.e. Conceptual, Analytical and Problem Solving, these sections are comprised of questions that help to understand the extent to which a user has understood the concepts. Now once the user has solved all the questions and has written the conclusion so then the user can click on the Submit option and he/she will get a copy of his response in the form of a PDF file. </p>

##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:
Challenges :The simulator page of the experiment consists of an assessment test given below the simulator window. The idea behind this is that user will solve the questions on simulator and it will help to understand the extent to which the user has understood the concepts. The provided questions test users understanding in three cognitive levels i.e. Conceptual, Analytical and Problem Solving.
1. Find the output voltage of DAC corresponding to the input 1111 0000.
2. If Vref range is 5v ,ADC is 8bit. What will be the input value for getting output of 30H.
3. State the difference between sensitivity and resolution of DAC.
4. Write a program which will read any four values from ADC and largest number will be copied to location 30H (Use debug option in simulator for providing four inputs to 8051 by ADC).
5. For 0 to 10v scale, Use ADC to calculate the difference between 8v and 3v, store result in 33H.
6. Take the Vref scale of 10V for ADC, calculate the minimum input voltage required to cause one bit change on output at a time, Do same for scale of 5V. Comment on results with different reference voltages.
7. Write a code such that, if ACC is less than 7FH, port2 will generate a digital code such that output of DAC will be 4V output, else clear the Port 2 (5v-0v is the range).

Questions :
1. Increase in one bit at the output of ADC occurs when input increase by:
a) (Vlow-Vhigh)/(2^n)
b) (Vlow-Vhigh)*(8)
c) (Vlow+Vhigh)/(8)
d) (Vlow-Vhigh)/(7)

2. The resolution of the ADC used, is of:
a) 7 bit resolution
b) 8 bit resolution
c) 0.0196v resolution
d) All of the above

3. An ADC is used for:
a) Generating analog signals
b) Converting digital signals to analog signals
c) Converting analog signals to digital signals
d) All of the above

4. Formula to find the resolution of ADC: (n is the resolution in bits)
a) (Vhigh)*(2^n)
b) (Vhigh-Vlow)/(2^n)
c) (Vhigh)/(2^n)
d) (Vhigh+Vlow)/(2*n)

5. From Q1 and Q4 it can be stated that:
a) Resolution is the smallest change in output for a discrete change in input
b) Vhigh-Vlow has no effect
c) Both a and b
d) None of the above

6. The DAC used in the experiment is of
a) 128 bit resolution
b) 8 bit resolution
c) Both a and b
d) 7 bit resolution

##### 2.5 Allow pitfalls:
1. An invalid assembly language instruction in the code written by the user may cause an error. The user must have to use the valid instruction set of 8051.
2. When a user tries to use infinite loop code (i.e. HERE : JNB P0.0 HERE), which relies on external input(s) to break the loop, the site will hang, prompting you to reload the site. Thus, user can understand from the mistake that an infinite loop code must not be written, instead an alternative must be found to achieve the goal.
3. Software generated delay programs will not work in given simulator. This is because the multiple iterations of loop executing in delay-program makes the web page unresponsive. Instead an alternative must be found to achieve the goal. In such case Debug option is suggested.
4. Writing a code taking into consideration a specific port and not selecting the specific port below the peripheral in the simulator will result in unexpected output. So user must take into consideration the usage of same port in the peripheral section as well as while writing the code.

##### 2.6 Conclusion:
1. The user will have to give answers to the questions asked in the 3 sections available just below the simulator, these questions have been asked in a tricky way. This will ensure that the use of this 8051 microcontroller lab is truly checking the understanding capacity of a user. The response of the user for the questions will be obtained in the form of a PDF file, the assessor can then assign marks for the answered questions.
2. Assessment/evaluation of the post-test should be given immediately to the user. The moment the student clicks on the answer of his/her choice, the CORRECT ANSWER should be displayed below the question. This would enable the student to understand whether he/she is right or wrong there and there itself.
3. The pre-test and post-test can actually give an idea to the user about his/her understanding of the specific experiment.
4. Student will learn to interface and program 8051 microcontroller in assembly language without hardware setup. This would enable the students to design their own application using 8051 microcontroller and ADC-DAC.

##### 2.7 Equations/formulas: NA

### 3. Flowchart:
<img src="flowchart/flowchart.png"/><br>
<a href="https://docs.google.com/drawings/d/17e5lMw8hngK9H7-5pFZGBEYoVHt1d5clhJ9OpxHDiV0/edit?usp=sharing" target="_blank">Click for flowchart</a>
### 4. Mindmap:
<img src="mindmap/mindmap.png"/><br>
<a href="mindmap/mindmap.pdf" target="_blank" >Click for mindmap</a>

### 5. Storyboard:
<img src="Storyboard/storyboard.gif"/><br>
<a href="Storyboard/storyboard.gif" target="_blank" >[here]</a>
