## Round 0
<b>Discipline | <b>Electronics & Telecommunication, Computer Science
:--|:--|
<b> Lab | <b> 8051 Microcontroller and Applications Lab
<b> Experiment|     <b> 1. Microcontroller interfaced with display devices.

<h5> About the Experiment : </h5> <br>

To understand the interfacing of 8051 Microcontroller with various display devices.
This experiment includes the interfacing of LEDs and Seven segment display with 8051 Microcontroller.
After completion of this experiment, students will be able to
       1.	Interface a 8051 microcontroller with a display device and can perform a desired task. 
       2.	Program a 8051 microcontroller using assembly language.


<b>Name of Developer | <b>Mrs.Anita S.Diwakar
:--|:--|
<b> Institute | <b> Indian Institute of Technology Bombay
<b> Email id|     <b> anitasd [at] iitb [dot] ac [dot] in
<b> Department | IDP in Educational Technology

#### Contributors List

SrNo | Name | Faculty or Student | Department| Institute | Email id
:--|:--|:--|:--|:--|:--|
1 | Kedar Pednekar | Student | Electronics and Telecommunication | V.E.S. Institute of Technology, Mumbai | 2015kedar.pednekar@ves.ac.in
2 | Adesh Chavan | Student | Electronics and Telecommunication |  V.E.S. Institute of Technology, Mumbai | 2015adesh.chavan@ves.ac.in
3 | Abhishek Bane| Student | Electronics | V.E.S. Institute of Technology, Mumbai| ej2012.abhishek.bane@ves.ac.in
4 | Siddhesh Mhadnak | Student | Information Technology | V.E.S. Institute of Technology, Mumbai| siddhesh.mhadnak@ves.ac.in
5 | Abhinav Valecha | Student | Information Technology | V.E.S. Institute of Technology, Mumbai| abhinav.valecha@ves.ac.in
6 | Ankit Kesharwani | Student | Information Technology | V.E.S. Institute of Technology, Mumbai| ankit.kesharwani@ves.ac.in
7 | Jai Mathur | Faculty | Chemical Engineering | IIT Bombay | mathurjai88@gmail.com
<br>
