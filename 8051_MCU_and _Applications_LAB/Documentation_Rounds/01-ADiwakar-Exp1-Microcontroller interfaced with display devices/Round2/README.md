## Round 2


Experiment 1: Interfacing of 8051 Microcontroller with various display devices.

### 1. Story Outline:
<p align="justify">The experiment comprises of peripherals that are been interfaced with a 8051 Microcontroller. Here there are peripherals like LEDs and 7 segment display. Now so as to see the output it is essential that a code must be written using the Assembly Language. There are 4 ports i.e. Port 0,1,2 and 3. The user can select any port as per their convenience. Once the code is written then for line by line execution a user has to simply use the Debug option for execution of an individual line. Finally the output will be visible on the peripheral that is interfaced with the microcontroller. So the user will understand the interfacing of display devices with 8051 microcontroller.</p>

### 2. Story:
The theme of experiment is to simulate and analyze the operation of 8051 Microcontroller behaviour with Display devices: When a user writes an assembly language program ,at output we get some pins of microcontroller at digital high logic. So that corresponding segments of seven segment display glows and a meaningful pattern can be generated and the changes in the values of register architecture can be analyzed to understand the overall working of 8051 IC when programmed in Assembly language. The design of the simulation experiment is narrated as a story wherein the description of the visual stage, the goals and objectives planned and the pathway set for the learners’ are elaborately described. Moreover a few challenges and pitfalls are also set to underline and emphasize the concepts involved in the experimentation. Every stage is described thoroughly in the following section.

##### 2.1 Set the visual stage description:

<p align="justify"> When the user visits the simulator section, a simulation window will be visible. An important message window will pop-up, a user must make sure that the he/she should properly understand the message before performing any task on the simulator.

The simulator window consists of pin diagram of 8051 microcontroller, memory, peripheral (Seven Segment Display) and an editor window. The 8051 has 4 ports with a LED connected across each pin of the microcontroller, the LEDs indicate if a logic "HIGH" or "LOW" is applied to a specific port's pin.

The memory consists of several registers and the locations, the values stored in them will be visible in the memory.

The peripheral interfaced with 8051 is actually a Seven Segment Display, there will be a port across which this peripheral will be connected. Seven Segment Display is an arrangement of seven LEDs in a manner that they can display alphanumeric pattern.

The editor window is on the extreme right, a user must write a code, the assembly language should be used. A sample code has been provided so as to ensure that the user will get an idea about the experiment. The user can write their own code, this code will actually act like an input. User can create many application scenarios and write code for it.
</p>


##### 2.2 Set User Objectives & Goals:
1. The prime objective of the experiment is to demonstrate the working of 7 segment display.
2. To understand how a specific number or character can be made visible on the 7 segment display.
3. To understand the basics of assembly coding using the sample code provided.
4. To apply a new logic in the coding apart from the sample code, writing a code to achieve the goal (displaying a number or character on the display).
5. To know that other port can be used apart from the default port.
6. To understand that there are two types of displays i.e. common cathode and common anode display.
7. To understand whether a HIGH (i.e. 1) or a LOW (i.e. 0) has to be applied to a segment so as to make that specific segment glow. This objective can be easily achieved through observation while performance.
8. To compare the working of 7 segment display and a LED in the real life and on the virtual platform, analyze the difference observed and comment on the same with a fundamental reasoning.
9. Attempt the assessment questions, there are 3 sections i.e. Conceptual, Analytical and Problem Solving. Each category will check the understanding of a user. For 100 % completion a user must solve all the questions by selecting each section and writing the conclusion.
10. Finally click on the "Submit Answer" so that a PDF can be generated of the user's response. The assessor then can evaluate the marks scored by the user.

##### 2.3 Set the pathway activities:
1. <p align="justify">The set-up consists of a microcontroller interfaced with peripherals (Display devices). In this experiment a LED is connected to each pin of 8051 and there is a 7 segment display too. If the user wants to change the port then he/she can do it as per his/her convenience. Initially an important message will pop up, users must read it and understand that the simulator has some boundaries and so a user should work within these boundaries. </p>
2. <p align="justify">There is an editor visible in the simulator window, it is expected that a user must write an assembly language code so as to verify the working of the peripherals interfaced with microcontroller.  </p>
3. <p align="justify">There are few codes provided as sample or as a reference for a user. This code can be copied and pasted in the editor so as to observe the working of the peripheral. Users can apply their own logic and explore different ways to achieve the common goal of observing the working of the peripheral. </p>
4. <p align="justify">There are three options provided at the bottom of the editor. The first option is Run, simply by pressing this option the entire code will be executed at once. </p>
5. <p align="justify">The second option is Debug, this option can come in handy in case if a user is interested to observe the execution of each line in the written code. </p>
6. <p align="justify">The third option is Reset, this option resets the microcontroller by resetting all the registers to their default values. </p>
7. <p align="justify">If the code written by the user has some syntax errors as per the instruction set of 8051, the simulator will highlight those lines with corresponding error. </p>
8. <p align="justify">So the user will have to rectify it with appropriate instruction and run the program again. This makes their assembly programming more clear. </p>
6. <p align="justify">After running their program, user will analyze the status of corresponding pins, register values in architecture, output pattern generated at seven-segment display. </p>
9. <p align="justify">After performance a user can print the simulator window and take it's printout by simply clicking the Print Simulation Window option. </p>
10. <p align="justify">There are 3 sections i.e. Conceptual, Analytical and Problem Solving, these sections are comprised of questions that help to understand the extent to which a user has understood the concepts. Now once the user has solved all the questions and has written the conclusion so then the user can click on the Submit option and he/she will get a copy of his response in the form of a PDF file. </p>

##### 2.4 Set Challenges and Questions/Complexity/Variations in Questions:
Challenges : The simulator page of the experiment consists of an assessment test given below the simulator window. The idea behind this is that user will solve the questions on simulator and it will help to understand the extent to which the user has understood the concepts. The provided questions test users understanding in three cognitive levels i.e. Conceptual, Analytical and Problem Solving.
1. State the use of seven segment.
2. Write a program to turn ON the LEDs connected across the even bits of port 1 and the odd bits of port 3.
3. If 42H and 35H are added together then the obtained result should be displayed across the LEDs connected across port 0. Now interchange the nibbles of this obtained result and it should be displayed across port 2.
4. Write a program to display vowels on seven segment display, mention which vowel character can not be displayed.
5. How can you glow 10 LEDs simultaneously or show 12 bit binary 0010 1111 0011 on port 1 and port 3?
6. If 25H is a given BCD input then find the excess-3 code of the same value and display it across the LEDs connected to port 0.

Questions :

1. If the junction temperature of LED is increased, the radiant output power __________________.
a) Increases
b) Decreases
c) Remains the same
d) None of the above

2. An eight segment is present in the seven segment display, what is the use?
a) Has no use
b) For displaying non-integer numbers
c) For displaying integer numbers
d) None of the above

3. To display a digit on seven-segment display, we have to apply its __________ at the input.
a) 8 bit binary equivalent
b) Grey code
c) BCD
d) None of the above

4. A LED is forward-biased. The diode should be ON, but no light is visible.  What can be the possible reason for it?
a) The applied voltage is too small
b) The series resistor is too small
c) The applied voltage is too high
d) None of the above

##### 2.5 Allow pitfalls:

1. An invalid assembly language instruction in the code written by the user may cause an error. The user must have to use the valid instruction set of 8051.
2. When a user tries to use infinite loop code (i.e. HERE : JNB P0.0 HERE), which relies on external input(s) to break the loop, the site will hang, prompting you to reload the site. Thus, user can understand from the mistake that an infinite loop code must not be written, instead an alternative must be found to achieve the goal.
3. Software generated delay programs will not work in given simulator. This is because the multiple iterations of loop executing in delay-program makes the web page unresponsive. Instead an alternative must be found to achieve the goal. In such case Debug option is suggested.
4. Writing a code taking into consideration a specific port and not selecting the specific port below the peripheral in the simulator will result in unexpected output. So user must take into consideration the usage of same port in the peripheral section as well as while writing the code.

##### 2.6 Conclusion:
1. The user will have to give answers to the questions asked in the 3 sections available just below the simulator, these questions have been asked in a tricky way. This will ensure that the use of this 8051 microcontroller lab is truly checking the understanding capacity of a user. The response of the user for the questions will be obtained in the form of a PDF file, the assessor can then assign marks for the answered questions.
2. Assessment/evaluation of the post-test should be given immediately to the user. The moment the student clicks on the answer of his/her choice, the CORRECT ANSWER should be displayed below the question. This would enable the student to understand whether he/she is right or wrong there and there itself.
3. The pre-test and post-test can actually give an idea to the user about his/her understanding of the specific experiment.
4. Student will learn to interface and program 8051 microcontroller in assembly language without hardware setup. This would enable the students to design their own application using 8051 microcontroller and display devices.

##### 2.7 Equations/formulas: NA

### 3. Flowchart:
<img src="flowchart/flowchart.png"/><br>
<a href="https://docs.google.com/drawings/d/1unnH7aaUQmV5TSaXsyds7IzUlVeNlsyIeY5g1w08RK0/edit?usp=sharing" target="_blank">Click for flowchart</a>

### 4. Mindmap:
<img src="mindmap/mindmap.png"/><br>
<a href="mindmap/mindmap.pdf" target="_blank">Click for mindmap</a>

### 5. Storyboard:
<img src="Storyboard/storyboard.gif"/><br>
<a href="Storyboard/storyboard.gif" target="_blank">[here]</a>
