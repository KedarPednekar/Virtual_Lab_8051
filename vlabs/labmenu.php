<html><head>
    <title>Virtual Labs</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="bootstrap/css/labmenu.css" rel="stylesheet" type="text/css">
  </head><body>
    <?php include 'common/header.html'; ?>
    <div class="section section-primary">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="jumbotron">
              <h1 class="text-primary">8051 Microcontroller and Applications<!-- Lab Name --></h1>
              <p class="text-primary">Users will be able to write a program and see the output across the peripherals interfaced with the 8051 microcontroller.<!-- Description --></p>
              <a class="btn btn-primary btn-large" href="labs/explist.php">Explore</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p contenteditable="true">
              <br>
            </p>
            <br></br>


<center>

             <h3>About VLAB</h3> 

 <br></br>

              <h3>This lab is contributed by <!-- Institute Name --></h3> 
              <p></p>
               <img src="images\iitb_logo_30dpi.png" alt="" height="100" width="200"> <!-- image link-->
              <p></p>
              
            </center>
          </div>
        </div>
      </div>
    </div>
    
  <?php include 'common/footer.html'; ?>

</body></html>