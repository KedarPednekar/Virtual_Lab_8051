<?php
 

if(isset($_POST['submit']))
	{
		
		
		$ans1=$_POST['ans1'];
		$ans2=$_POST['ans2'];
		$ans3=$_POST['ans3'];
		$ans4=$_POST['ans4'];
		$ans5=$_POST['ans5'];
		$ans6=$_POST['ans6'];
		$ans7=$_POST['ans7'];
		$ans8=$_POST['ans8'];
		$ans9=$_POST['ans9'];
		$conclude=$_POST['conclude'];   // taking user submitted answers in php variables from question-paper.php file
		
		
		for($i=1;$i<=9;$i++){ 
		$list[$i]= str_replace('<br>'," ",$list[$i]);
		}
		
	
		require('fpdf181/fpdf.php'); // fpdf library

			class PDF extends FPDF
			{
				// Page header
				function Header()
				{
					// Logo
					//$this->Image('logo.png',10,6,30);
					// Arial bold 15
					$this->SetFont('Arial','B',15);
					// Move to the right
					$this->Cell(80);
					// Title
					$this->Cell(30,15,'Test Sheet',1,0,'C');  // HEADING for Answer Sheet
					// Line break
					$this->Ln(20);
				
				}

				// Page footer
				function Footer()
				{
					// Position at 1.5 cm from bottom
					$this->SetY(-15);
					// Arial italic 8
					$this->SetFont('Arial','I',8);
					// Page number
					$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
				}
				
				function ChapterBody($ans) // for multi line paragraph with auto line break for new line
				{
					// Read text file
					$txt = '--> '.$ans;
					// Times 12
					$this->SetFont('Times','',15); // set font size for answers here 
					// Output justified text
					$this->MultiCell(0,5,$txt);
					// Line break
					$this->Ln();
					// Mention in italics
					$this->SetFont('','I');
					
				}
						
				function PrintChapter($ans)   // buffer function 
				{
				   // $this->AddPage();
				   // $this->ChapterTitle($num,$title);
					$this->ChapterBody($ans);
				}										
			}
	
			// Instanciation of inherited class
			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();
				
				$pdf->SetFont('Times','B',20); // font size of headings
				$pdf->Cell(40,15,'Conceptual Questions:-',0,0,'L'); // single line heading in BOLD 
				$pdf->Ln();     
				
				$pdf->SetFont('Times','',18);   // font size of questions 
				$pdf->MultiCell(0,5,'Q1)'.$list[1],0,'J');  // Question and answer one below another
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans1);    //multi line answer content with auto newline break  
				$pdf->Ln();                   // new line 
				
				$pdf->SetFont('Times','',18); // font size of questions
				$pdf->MultiCell(0,5,'Q2)'.$list[2],0,'J'); //multi line question2
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans2);    //multi line answer2 
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18); // font size of questions
				$pdf->MultiCell(0,5,'Q3)'.$list[3],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans3);   //multiline answer
				$pdf->Ln();
				
				$pdf->SetFont('Times','B',20);  // font size of heading
				$pdf->Cell(40,15,'Problem Solving Questions:-',0,0,'L');
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);   // font size of questions
				$pdf->MultiCell(0,5,'Q4)'.$list[4],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans4);
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);
				$pdf->MultiCell(0,5,'Q5)'.$list[5],0,'J');         // same as above
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans5);
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);
				$pdf->MultiCell(0,5,'Q6)'.$list[6],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans6);
				$pdf->Ln(); 
				
				
				$pdf->SetFont('Times','B',20);
				$pdf->Cell(40,15,'Analytical Questions:-',0,0,'L');
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);
				$pdf->MultiCell(0,5,'Q7)'.$list[7],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans7);
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);
				$pdf->MultiCell(0,5,'Q8)'.$list[8],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans8);
				$pdf->Ln();
				
				$pdf->SetFont('Times','',18);
				$pdf->MultiCell(0,5,'Q9)'.$list[9],0,'J');
				$pdf->Ln();  $pdf->Ln();  
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($ans9);
				$pdf->Ln();
				
				$pdf->SetFont('Times','B',20);
				$pdf->MultiCell(0,10,'CONCLUSION',0,'J'); // heading of conclusion
				$pdf->SetFont('Times','');
				$pdf->PrintChapter($conclude);   // multi line conclusion 
				$pdf->Ln();
				
			
				$pdf->Output("Submitted_TestPaper.pdf","D");  // D for forced download ,,I for inline browser 
				// submitted_testpaper.pdf here is a name given to generated PDF, it is user defined
													
				// FOR MORE INFORMATION ABOUT FUNCTIONS RELATED TO FEATURES OF FPDF LIBRARY AND TUTORIALS REFER http://www.fpdf.org/ 
	}
	
	
?>