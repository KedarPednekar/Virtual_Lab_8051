<?php 
//Conceptual questions
$Cquestion[1]="Implement a logic that will rotate a single HIGH bit around Port1 using debug option in the simulator. ";
$Cquestion[2]="Write a program in such a way that 27H and 45H should be visible across port0 and port1 respectively, now add them. \n The result should be visible across port2.";
$Cquestion[3]="Write a program to turn ON the LEDs connected across the even bits of port 1 and the odd bits of port 3.";
$Cquestion[4]="Write a program such that one port will accept digit code as input and display the corresponding number on seven segment display \n connected on the other port.";
$Cquestion[5]="If (5B)H is given as input to a port so as to display '5' on a seven segment display under common cathode configuration \n ,then what input should be given to the port so as to display '5' under common anode configuration?";
$Cquestion[6]="State the use of seven segment.";
$Cquestion[7]="How to verify whether the seven segment display visible in the simulator is a common anode or a common cathode type?";
$Cquestion[8]="What does the common cathode mean?"; 
$Cquestion[9]="List various display devices with their advantages over others.";

//Problem solving questions
$Pquestion[1]="Implement a logic such that a johnson counter will form on port0 using debug option in the simulator.";
$Pquestion[2]="In the simulator we apply LOGIC ONE  on a pin to glow respective LED as cathode is assumed to be grounded and anode is connected to the pin.\n Is it possible to glow the led by applying LOGIC ZERO to respective pin? what will be the change in the hardware.";
$Pquestion[3]="If 42H and 35H are added together then the obtained result should be displayed across the LEDs connected across port 0. \n Now interchange the nibbles of this obtained result and it should be displayed across port 2. ";
$Pquestion[4]="When is the 8th segment(decimal point) used in the seven segment? Is it really essential? If yes, then justify.";
$Pquestion[5]="Write a program to display even numbers between 0-9.";
$Pquestion[6]="Write a program to display odd numbers between 0-9.";
$Pquestion[7]="Write a program to display numbers horizontally flipped(eg: 3 = E).";
$Pquestion[8]="If 23H and 15H are added together then the obtained result should be displayed across the LEDs connected across port 2. \n Now interchange the nibbles of this obtained result and it should be displayed across port 0. ";
$Pquestion[9]="If 08H and 24H are added together then the obtained result should be displayed across the LEDs connected across port 1. \n Now interchange the nibbles of this obtained result and it should be displayed across port 0. ";
$Pquestion[10]="Write a program to display vowels on seven segment display, mention which vowel character can not be displayed.";

// Analytical questions
$Aquestion[1]="How can you glow 10 LEDs simultaneously or show 12 bit binary 0010 1111 0011 on port 1 and port 3?";
$Aquestion[2]="Write a program in such a way that 45H should be visible across port 0 and the 1's complement of 45H should be visible across port2.";
$Aquestion[3]="Write a program in such a way that 64H should be visible across port 3 and the 2's complement of 64H should be visible across port1.";
$Aquestion[4]="If 25H is a given BCD input then find the excess-3 code of the same value and display it across the leds connected to port 0.";
$Aquestion[5]="If 55H and 22H are the two given inputs then multiply them,<br>the content of A register and B register should be given to port 2 and port 0 respectively.";
$Aquestion[6]="Swap the lower nibble with upper nibble of port0 without using SWAP instruction.State the idea you have applied.";
$Aquestion[7]="Write a program to display '3' on the seven segment display using 'ADD' instruction. \n (Hint : Refer common cathode display decoding table given in the theory section).";
$Aquestion[8]="Write a program to display '9' on the seven segment display using 'SUBB' instruction. ";
$Aquestion[9]="So as to glow a specific segment in the 7 segment display what logic should be applied to the corresponding pin \n  in the common cathode configuration?";
$Aquestion[10]="The excess 3 code of binary number 01001100 is ________ ? Display the obtained number on seven segment display.";
$Aquestion[11]="Write a program to display 'LAbS'. (sequentially one character after another)";
						
		$y=range(1,9); // total number of questions currently in Conceptual type 
		shuffle($y);
		for ($i=$n=1; $i<=3; $i++) {    //  change here if want to increase number of questions of a particular type 
			  $list[$i]=$Cquestion[$y[$i-$n]];    // list[$i] contains question strings ,here first three questions are of conceptual type
								}                 // list[0] is left empty for simplicity of further programs and understanding 
			
		$y=range(1,10); // total number of questions currently in Problem Solving type 
		shuffle($y);
		for ($i=$n=4; $i<=6; $i++) {    // next 3 questions  (4th to 6th)
			  $list[$i]=$Pquestion[$y[$i-$n]];   // problem solving type questions 
								}    
								
		$y=range(1,11);  // total number of questions currently in Analytical type 
		shuffle($y);
		for ($i=$n=7; $i<=9; $i++) {  //next 3 questions (7th to 9th)
			  $list[$i]=$Aquestion[$y[$i-$n]];  //problem solving type questions  
								}    
		//list is an output array containing three questions of each type from 1st to 9th location of list array. 0th location is kept empty for simple programming 
		// hence first question will always be on second location of list array,i.e- list[1], for easy manupulation. 
		
		
		session_start();
		$_SESSION['question_list']= $list;	//for sending question array (list[]) to question paper displaying file question-paper.php file	
?>
		<html> <body>
		<iframe height="1700" width="1080" src="question-paper.php" frameBorder="0"></iframe> <!-- use this iframe in every questions.php file -->
		</body>	</html>			