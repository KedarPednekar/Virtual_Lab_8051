1)questions_n.php has complete pool of questions corresponding to nth experiment. this file will give few randomly picked questions in list[] array.
this file includes a question form file 'question-paper.php'.
2)The randomly picked questions are contained in a list[] array ,passed to Question-paper.php file
3)Question-paper.php will take the list[] array and displays as a question paper to student ,this file includes a pdfconverter.php file.
4)pdfconverter.php file contains code for converting randomly picked questions and user written answers into a PDF,, it uses FPDF library .
5)FPDF folder is the FPDF library used for PDF generation. for more information go to http://www.fpdf.org/
6)make more questions_n.php files for further experiments,keep pdfconverter.php and question-paper.php file common to all questions_n.php files