<?php session_start();
$list= $_SESSION['question_list']; // to take questions from appropriate question files
include 'pdfconverter.php'; //including pdf conversion file  ?>

<html>
<head></head>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../cssfiles/w3.css">  <!-- stylesheet for progress bar --> 

<style> <!-- CSS required for tabs style -->

body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}                          
<!-- CSS for tab style ends here -->
</style>



<body>

		<div class="w3-container">
		<br><h4><b>Instructions:- </b></h4> 
		<h5>1)There are three sections.(conceptual questions, problem solving questions, analytical questions)</h5>
		<h5>2)Click on Tabs to attempt questions in each section.</h5>
		<h5>3)Please attempt all the questions and write the conclusion before submitting.</h5>
		<h5>4)To check and update the progress bar,please click on any of the tab after attempting questions in a section.</h5>
		<h5>5)Please note:- To get 100% progress, attempting all the sections along with the conclusion is necessary.</h5>
		
		
		
		<br> <h4 align="center"> <b>Progress Bar</b> </h4>
			<div class="w3-light-grey">
			<div id="myBar" class="w3-container w3-green w3-center" style="width:0%">0%</div>
			</div>
			<div class="tab">						
									
		  <button class="tablinks" align="left" onclick="openQs(event, 'Conceptual')" >Conceptual</button>
		  <button class="tablinks" align="center" onclick="openQs(event, 'Problem solving')" >Problem solving</button> 
		  <button class="tablinks" align="right" onclick="openQs(event, 'Analytical')" >Analytical</button>
		   <!-- three tab button for types of questions -->
			</div>
									
		<form method="post">  <!--this is formed as a question paper with answer box for students -->
		
									
					<div id="Conceptual" class="tabcontent">
						Q1.<?php echo $list[1] ; ?>    
						<textarea rows="10" cols="110" name="ans1" id="test1"></textarea>
						<br>
						Q2.<?php echo $list[2] ; ?> 
						<textarea rows="10" cols="110" name="ans2" id="test2"></textarea> 
						<br>
						Q3.<?php echo $list[3] ; ?> 
						<textarea rows="10" cols="110" name="ans3" id="test3"></textarea> 
						<br>
					</div>
					
					<div id="Problem solving" class="tabcontent">

						Q4.<?php echo $list[4] ; ?> 
						<textarea rows="10" cols="110" name="ans4"id="test4"></textarea> 
						<br>
						Q5.<?php echo $list[5] ; ?> 
						<textarea rows="10" cols="110" name="ans5" id="test5"></textarea> 
						<br>
						Q6.<?php echo $list[6] ; ?> 
						<textarea rows="10" cols="110" name="ans6" id="test6"></textarea> 
						<br>
											
					</div>
					
					<div id="Analytical" class="tabcontent">
					
					 
							
						Q7.<?php echo $list[7] ;  ?> 
						<textarea rows="10" cols="110" name="ans7" id="test7"></textarea> 
						
						<br>
						Q8.<?php echo $list[8] ;   ?> 
						<textarea rows="10" cols="110" name="ans8" id="test8"></textarea> 
						<br>
						Q9.<?php echo $list[9] ;  ?>   
						<textarea rows="10" cols="110" name="ans9" id="test9"></textarea> 
						<br>
					</div>

								
					<h3> Conclusion</h3>
					<textarea rows="10" cols="110" name="conclude" id="conclude" font size ="3"></textarea>

					<br><br>
									

		</div>
							
				<br><br>     <h4> Click on submit to generate a PDF of the test. </h4>
							 <input type="submit" name="submit" value="Submit Answers">  <!-- this is submit button for making pdf -->
							 
		</form>		
						

							

<script>
function openQs(evt, qsType) 
	{    // for displaying particular questions on clicking corresponding TAB (conceptual,problem solving,analytical etc) 
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(qsType).style.display = "block";
			evt.currentTarget.className += " active";



		  var elem = document.getElementById("myBar");   
		  var width;   // parameters for progress bar
		  

		 var content1 = document.getElementById("test1").value;
		var content2 = document.getElementById("test2").value;
		var content3 = document.getElementById("test3").value;
		var content4 = document.getElementById("test4").value;
		var content5 = document.getElementById("test5").value;
		var content6 = document.getElementById("test6").value;
		var content7 = document.getElementById("test7").value;
		var content8 = document.getElementById("test8").value;
		var content9 = document.getElementById("test9").value;
		var content10 = document.getElementById("conclude").value;

		var blocks; // for calculating total number of attempted questions including conclusion text box.
		blocks= (content1.length>1)+(content2.length>1)+(content3.length>1)+(content4.length>1)+(content5.length>1)+(content6.length>1)+(content7.length>1)+(content8.length>1)+(content9.length>1)+(content10.length>1);   
			 
		   var q =10 ; // total number of questions including Conclusion(currently 9 questions ,1 conclusion) 
		   width= (blocks/q)* 100;    // to show progress bar in percentage %
		   
			 //document.getElementsByTagName("PROGRESS")[0].setAttribute("value",width);

			  elem.style.width = width + '%';    // show progress bar as number of attempted questions 
			  elem.innerHTML = width * 1  + '%';   
			
	}
</script>
				
</body>

</html>

