<?php 
//Conceptual questions 
$Cquestion[1]="What is resolution? Calculate the analog voltage required for one bit change in the output of ADC in simulator.";
$Cquestion[2]="List any five ADC and DAC ICs used in practice and state their principle of operation.";
$Cquestion[3]="List the practical examples of sensors and actuators that can replace the knob in the simulator.";
$Cquestion[4]="State the difference between sensitivity and resolution of DAC.";
$Cquestion[5]="What are the control signals of practical ADC 0808 and DAC 0808? State their functions.";
$Cquestion[6]="What are the ADC and DAC specifications that you know? "; 
$Cquestion[7]="What are the techniques you know that are used to convert Digital signal into Analog signal?";
$Cquestion[8]="State the expression for Resolution of DAC.";
$Cquestion[9]="State the expression for Resolution of ADC.";


//Problem solving questions
$Pquestion[1]="Find the output voltage of DAC corresponding to the input 1010 1101.";
$Pquestion[2]="For 0 to 5V scale, check the analog shift in output of DAC using INC instruction and debug option in the simulator .and note the output for 5 increments. \n Also state the term used for expressing the increment in analog output of DAC per bit change at input. ";
$Pquestion[3]="Write a program which will read any four values from ADC and largest number will be copied to location 30H \n (Use debug option in simulator for providing four inputs to 8051 by ADC).";
$Pquestion[4]="What will happen if the input/output of ADC /DAC is changed?";
$Pquestion[5]="Write a program which will read any five values from ADC and largest number will be copied to location 12H \n (Use debug option in simulator for providing four inputs to 8051 by ADC).";
$Pquestion[6]="Find the output voltage of DAC corresponding to the input 1111 0000.";
$Pquestion[7]="For 0 to 10v scale,Use ADC to calculate the difference between 8v and 3v, store result in 33H.";
$Pquestion[8]="For 0 to 15v scale,Use ADC to calculate the difference between 8v and 3v, store result in 20H.";
$Pquestion[9]="For 0 to 5v scale,Use ADC to calculate the difference between 4.2v and 3.1v, store result in 20H.";



//Analytical questions
$Aquestion[1]="Write a code such that, if ACC is less than 7FH, port2 will generate a digital code such that output of DAC will be 4V output, \n else clear the Port 2 (5v-0v is the range).";
$Aquestion[2]="If Vref range is 10v ,ADC is 8bit .What will be the input value for getting output of 66H or 01100110B. \n Also verify the formula :(ADCout)= Vin*256/Vref.";
$Aquestion[3]="State the difference between analog to digital conversion and decimal to binary conversion.";
$Aquestion[4]="If Vref range is 5v ,ADC is 8bit .What will be the input value for getting output of 30H.";
$Aquestion[5]="Write a code such that, if Accumulator is less than 0FH, port1 will generate a digital code such that output of DAC will be 3.8V, \n else output will be 5.0v.";
$Aquestion[6]="Take the Vref scale of 10V for ADC, calculate the minimum input voltage required to cause one bit change on output at a time, Do same for scale of 5V. Comment on results with different reference voltages.";



		$y=range(1,9); // total number of questions currently in Conceptual type 
		shuffle($y);
		for ($i=$n=1; $i<=3; $i++) {      //  change here if want to increase number of questions of a particular type 
			  $list[$i]=$Cquestion[$y[$i-$n]];    // list[$i] contains question strings ,here first three questions are of conceptual type
								}                 // list[0] is left empty for simplicity of further programs and understanding 
			
		$y=range(1,9); // total number of questions currently in Problem Solving type 
		shuffle($y);
		for ($i=$n=4; $i<=6; $i++) {    // next 3 questions  (4th to 6th)
			  $list[$i]=$Pquestion[$y[$i-$n]];   // problem solving type questions 
								}    
								
		$y=range(1,6);   // total number of questions currently in Analytical type 
		shuffle($y);
		for ($i=$n=7; $i<=9; $i++) {  //next 3 questions (7th to 9th)
			  $list[$i]=$Aquestion[$y[$i-$n]];  //problem solving type questions  
								}    
		//list is an output array containing three questions of each type from 1st to 9th location of list array. 0th location is kept empty for simple programming 
		// // hence first question will always be on second location of list array,i.e- list[1], for easy manupulation. 
			  
		session_start();
		$_SESSION['question_list']= $list;	//for sending question array (list[]) to question paper displaying file question-paper.php file																														 																	
?>

		<html> <body>
		<iframe height="1700" width="1080" src="question-paper.php" frameBorder="0"></iframe>   <!-- use this iframe in every questions.php file -->
		</body>	</html>			