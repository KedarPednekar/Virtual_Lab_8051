<html>
<font size="3" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <script>
            window.onload = function () {
                document.getElementById("procedure").className = "active treeview";
            }
        </script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

        <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with ADC and DAC --></a>
              </li>
              <li class="active">Procedure</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
            <!-- Content Wrapper. Contains page content -->
			  <div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				  <h1 align="center"><?php echo $exp_name?>
					<!-- Microcontroller interfaced with ADC and DAC -->
				  </h1>
				</section>
				<!-- Main content -->
				<section class="content" id="pro">
						  <h3 style="margin-top:5%">Procedure</h3>   <!-- put your procedure here-->
					
					<p class="MsoNormal" style="text-align:justify">
						  <br> 1. Go to the simulator section to perform an experiment.<br><br>
						  <br><left><img src="..\images\procedure_images\goto-adc.JPG" alt="goto" style="width:600px;height:350px;"></br><br>
						  <br>2. Read all the instructions popping up from simulator window carefully.</br><br>
							&nbsp;  Note:Here in this experiment,the simulator window can be used for DAC study when code is given to 8051 first.<br><br>
							&nbsp; and also the same simulator window can be used for ADC study when input knob is varried first. <br><br>
						 <br><left><img src="..\images\procedure_images\read-comn.jpg" alt="read" style="width:600px;height:350px;"></br><br>

						  <br>3. Click on show sample button provided below. Understand the interfacing diagram and sample code.</br><br>
						   <br><img src="..\images\procedure_images\sample-adac.JPG" alt="sample" style="width:600px;height:350px;"><br>
						 <br>4. Copy the assembly program from samples.</br><br>
						 <br><left><br><img src="..\images\procedure_images\copycode-dac.jpg" alt="copy" style="width:600px;height:350px;"></br>
						 <br>5. Paste in Text editor in simulator window or write your own assembly language code.</br><br>
						<br><left><img src="..\images\procedure_images\adac-code.JPG" alt="code" style="width:600px;height:350px;"><br> </br>
						 <br>6. Select appropriate Port according to your code.</br><br>
						 <br><left><img src="..\images\procedure_images\adac-port.JPG" alt="port" style="width:600px;height:350px;"></br><br>

						 <br>7. To check syntax errors on each line,debug option is provided.</br>
						 <br><left><img src="..\images\procedure_images\adac-debug.JPG" alt="debug" style="width:600px;height:350px;"></br><br>
						 <br>8. If your code output is depending on timing sequence,please use debug funtion.It will show changes in output step by step.</br><br>
						 <br>9. To get the final output,run the simulator after debugging and assembling the code with no error.</br><br>
						 <br><left><img src="..\images\procedure_images\adac-run.JPG" alt="run" style="width:600px;height:350px;"></br><br>
						 <br>10. Solve test questions given below the simulator section.</br><br>
						 <br>11. Submit the answers and a PDF will be generated.(rename it as per your Rollno and Experiment no.) </br><br>
						 <br><left><img src="..\images\procedure_images\print-comn.JPG" alt="print" style="width:600px;height:350px;"></br><br>
				 
					
				  
					</p>
				</section>
				<!-- /.content -->
			  </div>
      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
        </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>