<html>
<font size="3" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <script>
            window.onload = function () {
                document.getElementById("theory").className = "active treeview";
            }
        </script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

        <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with display devices --></a>
              </li>
              <li class="active">Theory</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
             <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 align="center"> <?php echo $exp_name?>
            <!-- Microcontroller interfaced with display devices -->
          </h1>
        </section>
				
				<!-- Main content -->
				<section class="content">
				  <h3 style="margin-top:5%">Theory</h3>

				<p class="MsoNormal" style="text-align:justify">

					<strong>Task 1:</strong> Study the variation of a knob position according to the input from 8051 using DAC.
					<p></p>
					<br><strong>Theory –</strong></br>
					<!--Theory content goes here -->
					<p style="text-align:justify"> A  D/A Converter is used when the binary output from a digital system is to be converted into its equivalent analog voltage or current.
						D/A converter produces an output current proportional to digital word given as input. Today Microcomputers are widely used for industrial control but they are digital systems, in many practical system we have to convert digital control codes in analog signal to control output devices like relays, motors, actuators ,also in communication system the end terminals are analog in nature.
					</p>
					
					<p></p>
						<br><strong>Types of D/A configuration :</strong></br> 
						1.	Binary weighted resistors DAC
						<br>2.	R-2R ladder DAC</br> 
						DAC 0808 is one of the popular DAC package based on R-2R technique.
						<p></p>

						<br><strong>BINARY-WEIGHTED RESISTOR DAC :</strong></br>
						<p style="text-align:justify">The binary-weighted-resistor DAC employs the characteristics of the inverting summer Op Amp circuit. In this type of DAC, the output voltage is the inverted sum of all the input voltages. If the input resistor values are set to multiples of two: 1R, 2R and 4R, the output voltage would be equal to the sum of V1, V2/2 and V3/4. V1 corresponds to the most significant bit (MSB) while V3 corresponds to the least significant bit (LSB). 
						</p>
						<br><center><img src="..\images\binary_weighted_resistor.jpg" alt="binary_weighted_resistor" style="width:600px;height:400px;"></br>
						<p><a href="http://www.daenotes.com/electronics/digital-electronics/digital-to-analog-converters" target="blank">Image source : www.daenotes.com</a></center>
						</p>
						<br>

						<strong>R-2R ladder DAC :</strong>
						<p style="text-align:justify">To overcome huge range of resistor used in weighted resistor D/A converter, R-2R ladder D/A converter is introduced.<br> 
						In case of R-2R ladder D/A converter, Resistors of only two value (R and 2R) are used. 
						</p>
						<br><center><img src="..\images\R-2R_ladder.jpg" alt="R-2R_ladder" style="width:600px;height:400px;">
						<p><a href="http://www.electronicsengineering.nbcafe.in/r-2r-ladder-da-converter/" target="blank">Image source : http://www.electronicsengineering.nbcafe.in</a></center>
						</p>
						<p></p>
						<br>
						<br><center><img src="..\images\pin_diagram_of_DAC.jpg" alt="pin_diagram_of_DAC" style="width:500px;height:400px;">
						<p><a href="http://www.kirklau.com/proj/dac0808.gif" target="blank">Image source : www.kirklau.com</a></center>
						</p>
						<p></p>
						<br>

						<br><center><img src="..\images\pin_description_of_DAC.jpg" alt="pin_description_of_DAC" style="width:600px;height:500px;">
						<p><a href="http://www.futurlec.com/ADConv/DAC0808.shtml" target="blank">Image source : www.futurlec.com</a></center>
						</p>

						<p></p>
						<br><strong>Task 2:</strong> Read an analog voltage at the input of ADC given as the knob position using 8051 microcontroller.</br>

						<p></p>
						<strong>Theory –</strong>

						<br><p style="text-align:justify">An Analog to Digital Converter (ADC) is a very useful feature that converts an analog voltage on a pin to a digital number. By converting from the analog world to the digital world, we can begin to use electronics to interface to the analog world around us (as most of physical world such as heat, temperature, light, sound are analog in nature).</p> 
						<strong>Two techniques are widely used for A/D conversion :</strong>
						<br>1.	Successive Approximation (SAR ADC-0808)</br>2.	Dual slope integrator 
						

						<br>ADC 0808/0809 is one of the popular ADC package based on SAR technique.</br>
						<p></p>
						<br><center><img src="..\images\pin_diagram_of_ADC.JPG" alt="pin_diagram_of_ADC" style="width:500px;height:400px;">
						<p><a href="https://www.engineersgarage.com/electronic-components/adc0808-datasheet" target="blank">Image source : www.engineersgarage.com</a></center>
						</p>
						<br>
						<p></p>
						<br><center><img src="..\images\pin_description_of_ADC.jpg" alt="pin_description_of_ADC" style="width:600px;height:500px;">
						<p><a href="https://www.engineersgarage.com/electronic-components/adc0808-datasheet"target="blank">Image source : www.engineersgarage.com</a></center></p>

						<p></p>

						<br></br>
						<strong>Successive Approximation (SAR) :</strong>
						<br><center><img src="..\images\successive_approximation.jpg" alt="successive_approximation" style="width:600px;height:500px;"></br>
						<p><a href="http://www.circuitstoday.com/analog-to-digital-converters-ad" target="blank">Image source : www.circuitstoday.com</a></center>
						</p>

						<p></p>
						<br><p style="text-align:justify">At the start of a conversion cycle, the SAR is reset by making the start signal (S) high. The MSB of the SAR (Q7) is set as soon as the first transition from LOW to HIGH is introduced. The output is given to the D/A converter which produces an analog equivalent of the MSB and is compared with the analog input Vin.
						If comparator output is LOW, D/A output will be greater than Vin and the MSB will be cleared by the SAR.
						If comparator output is HIGH, D/A output will be less than Vin and the MSB will be set to the next position (Q7 to Q6) by the SAR.</p>
						<p style="text-align:justify">According to the comparator output, the SAR will either keep or reset the Q6 bit. This process goes on until all the bits are tried. After Q0 is tried, the SAR makes the conversion complete (CC) signal HIGH to show that the parallel output lines contain valid data. The CC signal in turn enables the latch, and digital data appear at the output of the latch. As the SAR determines each bit,  digital data is also available serially. As shown in the figure above, the CC signal is connected to the start conversion input in order to convert the cycle continuously. 
						</p>
						<p style="text-align:justify">The biggest advantage of such a circuit is its high speed. It may be more complex than an A/D converter, but it offers better resolution.
						</p>
						<p></p>

						<strong>Dual Slope Integrator :</strong>
						<p style="text-align:justify">In dual slope type ADC, the integrator generates two different ramps, one with the known analog input voltage VA and another with a known reference voltage –Vref. Hence it is called a s dual slope A to D converter. The logic diagram for the same is shown below.
						</p>
						<p></p>
						<br><center><img src="..\images\dual_slope_integrator.png" alt="dual_slope_integrator" style="width:600px;height:300px;">
						<p><a href="http://www.electronics-tutorial.net/analog-integrated-circuits/data-converters/dual-slope-type-adc/index.html" target="blank">Image source : www.electronics-tutorial.net</a></center>
						</p>

						<br><p style="text-align:justify">The binary counter is initially reset to 0000; the output of integrator reset to 0V and the input to the ramp generator or integrator is switched to the unknown analog input voltage VA.
						The analog input voltage VA is integrated by the inverting integrator and generates a negative ramp output. The output of comparator is positive and the clock is passed through the AND gate. This results in counting up of the binary counter. </p></br>
						
						<p style="text-align:justify">The negative ramp continues for a fixed time period t1, which is determined by a count detector for the time period t1. At the end of the fixed time period t1, the ramp output of integrator is given by</br>
							∴VS=-VA/RC×t1</p>
						
						<br><p style="text-align:justify">When the counter reaches the fixed count at time period t1, the binary counter resets to 0000 and switches the integrator input to a negative reference voltage –Vref.
						Now the ramp generator starts with the initial value –Vs and increases in positive direction until it reaches 0V and the counter gets advanced. When Vs reaches 0V, comparator output becomes negative (i.e. logic 0) and the AND gate is deactivated. Hence no further clock is applied through AND gate. Now, the conversion cycle is said to be completed and the positive ramp voltage is given by
						∴VS=Vref/RC×t2
							<br>Where Vref& RC are constants and time period t2 is variable.</br>
							The dual ramp output waveform is shown below.
							<p></p>
							<br><center><img src="..\images\dual_ramp_output.png" alt="dual_ramp_output" style="width:600px;height:300px;">
								<p><a href="http://www.electronics-tutorial.net/analog-integrated-circuits/data-converters/dual-slope-type-adc/index.html" target="blank">Image source : www.electronics-tutorial.net</a></center>
								</p>
							</p>
							
						<br><p style="text-align:justify">Since ramp generator voltage starts at 0V, decreasing down to –Vs and then increasing up to 0V, the amplitude of negative and positive ramp voltages can be equated as follows.</br>
						∴Vref/RC×t2=-VA/RC×t1
						<br>∴t2=-t1×VA/Vref</br>
						∴VA=-Vref×t1/t2</p>
						<br><p style="text-align:justify">Thus, the unknown analog input voltage VA is proportional to the time period t2, because Vref is a known reference voltage and t1 is the predetermined time period.
						The actual conversion of analog voltage VA into a digital count occurs during time t2. The binary counter gives corresponding digital value for time period t2. The clock is connected to the counter at the beginning of t2 and is disconnected at the end of t2.</br> 
						Thus the counter counts digital output as
						<br>Digital output=(counts/sec) t2</br>
						∴Digital output=(counts/sec)[t1×VA/Vref ] </p>
						<br><p style="text-align:justify">For example, consider the clock frequency is 1 MHz, the reference voltage is -1V, the fixed time period t1 is 1ms and the RC time constant is also 1 ms. Assuming the unknown analog input voltage amplitude as VA = 5V, during the fixed time period t1 , the integrator output Vs is</br>
						∴VS=-VA/RC×t1=(-5)/1ms×1ms=-5V
						<br>During the time period t2, ramp generator will integrate all the way back to 0V.</br>
						∴t2=VS/Vref ×RC=(-5)/(-1)×1ms=5ms=5000μs
						<br>Hence the 4-bit counter value is 5000, and by activating the decimal point of MSD seven segment displays, the display can directly read as 5V.</br>
							</p>

						<!-- theory ends here-->

						<br><br><br><br><br><br>
						<strong>Documentation about 8051 Microcontroller </strong> <br>
						<ul>
						<li><a href="..\..\src\pdfs-docs\8051 overview.pdf" target="blank">8051 Overview.pdf</a>   </li> <br>
						<li><a href="..\..\src\pdfs-docs\8051-1.pdf" target="blank">8051 Hardware Overview.pdf</a> </li> <br>
						<li><a href="..\..\src\pdfs-docs\8051IS.pdf" target="blank">8051 Instruction Set.pdf</a>   </li>
						</ul>
						<br>
		
				</p>
				</section>
        <!-- /.content -->
      </div>
      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
        </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>