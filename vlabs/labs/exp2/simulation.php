
<html>
<font size="3" >
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="vlab.css" type="text/css" >
		<meta name="keywords" content="Question Paper">
		
		
 <link rel="stylesheet" href="../../src/cssfiles/modal.css" type="text/css">	


        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <!-- Simulation scripts start-->
          <script src="../src/math.ob.js"></script>

          <script src="../src/numcheck.ob.js"></script>
          <script src="../src/canvasjschart.ob.js"></script>
          <script src="../src/bracket.ob.js"></script>
          <link href="../src/StyleSheet1.css" rel="stylesheet" />
        
		
		
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

     <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with display devices --></a>
              </li>
              <li class="active">Simulation</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
           <!-- Content Wrapper. Contains page content -->
      
						  
				<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1 align="center"><?php echo $exp_name?></h1>
			<!-- Microcontroller interfaced with display devices -->
							   
		</section>
							
	
			<script type="text/javascript">
									  // Popup window code
															function newPopup(url) {
															  popupWindow = window.open(url,'popUpWindow','height=500,width=400,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
															}
			</script>
			
			<section class="content-header" style="float:right; margin-top:2%">
							  <a href="JavaScript:newPopup('procedure.php');" style="color:green;font-size: 16px"><img src="../../dist/img/popout.png" style="height:20px; width:20px; "> Pop Up Procedure</a>
							  <br>
							  <br>
							  <a href="" style="color:green; font-size: 16px"><img src="../../dist/img/fork.png" style="height:20px; width:20px; "></a>
			</section>
							
							
							
							
							<!-- Main content -->

					<section class="content">
						<h3 style="margin-top:5%">Simulation</h3>
							  
   
								<!--Simulation content goes here -->
					<iframe id="print-iframe" src="../../src/simulator8051/index.html#/adc-dac" width="1090" height="710"></iframe> <!--Simulator link/Dir Here -->
					<br></br>

					
					<!-- button html-->
					<!-- Trigger/Open The Modal by TWO buttons-->
					<button id="myBtn1"><b>Show Sample: Data Converters-DAC</b></button>  <br><br>
					<button id="myBtn2"><b>Show Sample: Data Converters-ADC</b></button>
					

					<!-- The Modal -->
						<div id="myModal1" class="modal" >
						<style> .close {
										color: #aaaaaa;
										float: right;
										font-size: 60px;
										font-weight: bold;
									   }
						</style>
						  <!-- Modal content -->
							<div class="modal-content">
									<span id="close1" class="close">&times;</span>
                                     <p><h3>DAC interfaced with 8051</h3> </p>
									<p><img src="images\dacCkt.png" width=600px height=400px alt="DAC" align="left"> </p> <!-- put here dac image first -->
							
							<strong>DAC sample program :-</strong>
								<div align="center">
								<p> 
								(Use Debug option to see the changes in the position of the knob)
								<br><br>
								MOV P0,#0FFh   
								<br>
								MOV P0,#10h
								<br>
								MOV P0,#20h
								<br>
								MOV P0,#0EFh
								<br>
								MOV P0,#0AFh
								<br>
								MOV P0,#08h
								<br>
								MOV P0,#0BDh
								<br>
								MOV P0,#0FFh
								<br>
								<br><br>
								here notice that 0 is prefixed for A-F hex digits,i.e. 0EFh,0BDh
								</p>
								</div>
							
								<!-- sample code for adc-->
							</div> 
									
						</div> 			
					<!-- The Modal -->
						<div id="myModal2" class="modal">
				  <!-- Modal content -->
							<div class="modal-content">
							<span id="close2" class="close">&times;</span>

							<p><h3>ADC interfaced with 8051</h3> </p>
								<p><img src="images\adcCkt.png" width=600px height=400px alt="ADC" align="left"> </p>  <!-- put here the image link for adc-->					
								
								<p align="justify">
								
								As ADC is an input device to microcontroller, so we have to just move the knob of potentiometer or apply similar kind of analog sensor output to ADC which in turn gives digital input for the microcontroller.
								<br>
								Hence in this operation you just have to notice the analog to digital conversion and try to figure out what applications you can make from this kind of set-up.
							
								</p>
											<!-- sample code for adc-->
							</div> 					
						</div>				
					
							<script>
								window.onload = function () {
									document.getElementById("simulation").className = "active treeview";
								} 
											
											// modal js here
											// Get the modal
								var modal1 = document.getElementById('myModal1');

								// Get the button that opens the modal
								var btn1 = document.getElementById("myBtn1");

								// Get the <span> element that closes the modal
								var span1 = document.getElementById("close1");

								// When the user clicks the button, open the modal 
								btn1.onclick = function() {
									modal1.style.display = "block";
								}

								// When the user clicks on <span> (x), close the modal
								span1.onclick = function() {
									modal1.style.display = "none";
								}

								// When the user clicks anywhere outside of the modal, close it
								window.onclick = function(event) {
									if (event.target == modal1) {
										modal1.style.display = "none";
									}
								}
										// Get the modal
								var modal2 = document.getElementById('myModal2');

								// Get the button that opens the modal
								var btn2 = document.getElementById("myBtn2");

								// Get the <span> element that closes the modal
								var span2 = document.getElementById("close2");

								// When the user clicks the button, open the modal 
								btn2.onclick = function() {
									// console.log(9)
									modal2.style.display = "block";
								}

								// When the user clicks on <span> (x), close the modal
								span2.onclick = function() {
									modal2.style.display = "none";
								}

								// When the user clicks anywhere outside of the modal, close it
								window.onclick = function(event) {
									if (event.target == modal2) {
										modal2.style.display = "none";
									}
								}

							</script>    
		
								<!-- Simulation scripts end-->
								
						<br> </br>
						<button onclick="myFunction()"><b>Print Simulation Window</b></button>

							<script>
							function myFunction() {
								document.getElementById("print-iframe").contentWindow.print();  <!-- print only simulator frame-->
							}
							</script>

						<h3 style="margin-top:5%"> <b>TEST :- (Comprises of questions which are related to the ADC and DAC) </b></h3>
									   
										<!-- test questions here --> 
					
					   <iframe src="../../src/test-paper/questions_2.php" width="1225" height="1750"> </iframe> <!-- question set for Exp2 is projected here-->
						<br><br>
										<!-- Test Questions end here -->
										

					
					</section>
							<!-- /.content -->
				</div>

      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
    </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>