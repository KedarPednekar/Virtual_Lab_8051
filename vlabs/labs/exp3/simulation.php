<html>
<font size="3" >
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="vlab.css" type="text/css" >
		<meta name="keywords" content="Question Paper">
		
		 <link rel="stylesheet" href="../../src/cssfiles/modal.css" type="text/css">	
		
		
		
		
		
		
		

        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <!-- Simulation scripts start-->
          <script src="../src/math.ob.js"></script>

          <script src="../src/numcheck.ob.js"></script>
          <script src="../src/canvasjschart.ob.js"></script>
          <script src="../src/bracket.ob.js"></script>
          <link href="../src/StyleSheet1.css" rel="stylesheet" />
        
		
		
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

     <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with display devices --></a>
              </li>
              <li class="active">Simulation</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
           <!-- Content Wrapper. Contains page content -->
      
						  
						  <div class="content-wrapper">
							<!-- Content Header (Page header) -->
							<section class="content-header">
								<h1 align="center"><?php echo $exp_name?></h1>
							  <!-- Microcontroller interfaced with display devices -->
							</section>
							
							<script type="text/javascript">
							  // Popup window code
													function newPopup(url) {
													  popupWindow = window.open(url,'popUpWindow','height=500,width=400,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
													}
							</script>
							<section class="content-header" style="float:right; margin-top:2%">
							  <a href="JavaScript:newPopup('procedure.php');" style="color:green;font-size: 16px"><img src="../../dist/img/popout.png" style="height:20px; width:20px; "> Pop Up Procedure</a>
							  <br>
							  <br>
							  <a href="" style="color:green; font-size: 16px"><img src="../../dist/img/fork.png" style="height:20px; width:20px; "></a>
							</section>
							
							
							<!-- Main content -->

							<section class="content">
							  <h3 style="margin-top:5%">Simulation</h3>
							  
   
								<!--Simulation content goes here -->
								<iframe id="print-iframe" src="../../src/simulator8051/index.html#/motor" width="1090" height="710"></iframe> <!--Simulator link/Dir Here --> 
								<br></br>
								<!-- button html-->
								<!-- Trigger/Open The Modal -->
								<button id="myBtn"><b>Show Sample: DC Motor</b></button>

								<!-- The Modal -->
									<div id="myModal" class="modal">
										<style> .close {
												color: #aaaaaa;
												float: right;
												font-size: 60px;
												font-weight: bold;
											   }
										</style>
										<!-- Modal content -->
												<div class="modal-content">
												<span class="close">&times;</span>
												 <p><h3>DC Motor interfaced with 8051</h3> </p>
												<p><img src="images\motorCkt.png" width=600px height=520px alt="motorCkt" align="left"> </p> <!-- put here dc motor image first-->
												<strong>DC motor sample program :-</strong>
													<p>			
													(Use Debug option to know the concept of rotating a DC motor in a better way)
													<br><br>
													SETB P0.0  &nbsp;&nbsp;&nbsp;  //motor rotates in clockwise direction
													<br>	
													CLR P0.1
													<br>			
													SETB P0.0  &nbsp;&nbsp;&nbsp;  //motor stops rotating
													<br>		
													SETB P0.1
													<br>			
													CLR P0.0  &nbsp;&nbsp;&nbsp;  //motor rotates in anti-clockwise direction
													<br>		
													SETB P0.1 
													<br>			
													CLR P0.0  &nbsp;&nbsp;&nbsp;  //motor stops rotating
													<br>		
													CLR P0.1 
													</P>
												</div>
									   
										
													<script>
													window.onload = function () {
														document.getElementById("simulation").className = "active treeview";
													}  //for simulator
													
													// modal js here
													// Get the modal
													var modal = document.getElementById('myModal');

													// Get the button that opens the modal
													var btn = document.getElementById("myBtn");

													// Get the <span> element that closes the modal
													var span = document.getElementsByClassName("close")[0];

													// When the user clicks the button, open the modal 
													btn.onclick = function() {
														modal.style.display = "block";
													}

													// When the user clicks on <span> (x), close the modal
													span.onclick = function() {
														modal.style.display = "none";
													}

													// When the user clicks anywhere outside of the modal, close it
													window.onclick = function(event) {
														if (event.target == modal) {
															modal.style.display = "none";
														}
													}
													</script>
					
																<!-- Simulation scripts end-->
									
									</div>  
										<br><br><br></br>
										<button onclick="myFunction()"><b>Print Simulation Window</b></button>
										<script>
										function myFunction() {
											document.getElementById("print-iframe").contentWindow.print();  
										}
										</script>

										

							</section>
							<!-- /.content -->
						  </div>

      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
    </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>