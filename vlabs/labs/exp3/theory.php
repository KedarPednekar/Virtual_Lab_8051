<html>
<font size="3" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <script>
            window.onload = function () {
                document.getElementById("theory").className = "active treeview";
            }
        </script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

        <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with DC motor --></a>
              </li>
              <li class="active">Theory</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
             <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 align="center"> <?php echo $exp_name?>
            <!-- Microcontroller interfaced with DC motor -->
          </h1>
        </section>
       
	   <!-- Main content -->
        <section class="content">
          <h3 style="margin-top:5%">Theory</h3> <!--Theory content goes here -->

            <p class="MsoNormal" style="text-align:justify">

				<strong>Task 1:</strong> DC Motor interfacing with 8051 microcontroller
				 <p></p>
				<br><strong>Theory:-</strong></br>
				<p></p>
				<br><center><img src="..\images\dc_motor.jpg" alt="dc_motor" style="width:600px;height:400px;"></br>
				<p><a href="http://www.daenotes.com/electronics/digital-electronics/digital-to-analog-converters" target="blank">Image source : www.daenotes.com</a></center>
				</p>
				<br>A DC motor is an electrical mechanism that converts direct current electrical power into mechanical power. </br> 
				The principle of working of a DC motor is that "whenever a current carrying conductor is placed in a magnetic field, it experiences a mechanical force". The direction of this force is given by Fleming's left hand rule.

				<br></br><p></p>
				<br><center><img src="..\images\principle_of_working.jpg" alt="principle_of_working" style="width:700px;height:400px;"></br>
				<p><a href="http://www.motors-biz.com/news/newsDetail/466.html" target="blank">Image source : www.motors-biz.com</a></center>
				</p>

				<br>Hence by just reversing the voltage levels at the two pins of DC motor ,we can reverse the direction of rotation of the shaft.</br>
				If two pins are at same levels i.e. 0v-0v or 12v-12v , shaft will not move.
				<p></p>
				<br><strong>Need of motor driver :</strong></br>
				In practice 8051 microcontroller output pin sources low current not sufficient to drive the dc motor, also an isolation is required in interfacing of inductive devices. Hence, we normally use driver ICs such as ULN2003, L293D, transistors.
				<p></p>
				<br><center><img src="..\images\L293D.jpg" alt="L293D" style="width:700px;height:400px;"></br>
				<p><a href="http://www.motors-biz.com/news/newsDetail/466.html" target="blank">Image source : www.motors-biz.com</a></center>
				</p>
				<p></p>
				<br><strong>Motor drivers are used preferably to</strong></br>
				1. Increases voltage and current sourcing capability. 
				<br>2. Provides isolation to 8051 from inductive effects of motors.</br>
				3. Motor drivers has inbuilt H bridges so as to reverse the direction of rotation, no rewiring is required.
				<br>4. En pin is provided for PWM speed control.</br>   


				<p></p>
				<strong>Interfacing diagram of DC motor with 8051 Microcontroller</strong>
				<p></p>
				<br><center><img src="..\images\interfacing_of_DC_motor.jpg" alt="interfacing_of_DC_motor" style="width:700px;height:400px;"></br>
				<p><a href="http://www.circuitstoday.com/interfacing-dc-motor-to-8051" target="blank">Image source : www.circuitstoday.com</a></center>
				</p>


					<!-- theory ends here -->
				<br><br><br><br><br><br>
				<strong>Documentation about 8051 Microcontroller </strong> <br>
				<ul>
				<li><a href="..\..\src\pdfs-docs\8051 overview.pdf" target="blank">8051 Overview.pdf</a>   </li> <br>
				<li><a href="..\..\src\pdfs-docs\8051-1.pdf" target="blank">8051 Hardware Overview.pdf</a> </li> <br>
				<li><a href="..\..\src\pdfs-docs\8051IS.pdf" target="blank">8051 Instruction Set.pdf</a>   </li>
				</ul>
				<br>


            </p>
        </section>
        <!-- /.content -->
      </div>
      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
        </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>