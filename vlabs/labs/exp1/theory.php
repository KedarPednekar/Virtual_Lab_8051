<html>
<font size="3" >   
   <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title> Virtual Labs </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
        <script>
            window.onload = function () {
                document.getElementById("theory").className = "active treeview";
            }
        </script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <?php
        include '../../common/header.html';
        include 'lab_name.php';
        $lab_name = $_SESSION['lab_name'];
        $exp_name = $_SESSION['exp_name'];
        ?>

        <div class="wrapper">
        <header class="main-header">
        <!-- Logo -->
        <a href="../explist.php" class="logo">
        <p align="center" style="font-size:1em;"><b><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></b></p>
    </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
          <section class="content-header">
            <ol class="breadcrumb">
              <li>
                <a href="../explist.php"><i class="fa fa-dashboard"></i><?php echo $lab_name?><!-- 8051 Microcontroller and Applications Lab --></a>
              </li>
              <li>
                <a href="#"><?php echo $exp_name?><!-- Microcontroller interfaced with display devices --></a>
              </li>
              <li class="active">Theory</li>
            </ol>
          </section>
        </nav>
      </header>
            <?php include 'pane.html'; ?>
             <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 align="center"> <?php echo $exp_name?>
            <!-- Microcontroller interfaced with display devices -->
          </h1>
        </section>
		
        <!-- Main content -->
        <section class="content">
          <h3 style="margin-top:5%">Theory</h3>

            <p class="MsoNormal" style="text-align:justify">

			<h4><strong>Task 1:</strong></h4>  Blinking LED using 8051 Microcontroller.
			<p></p>
			<br><h4><strong>Theory –</strong></h4>
			
			<!--Theory content goes here -->

					<p style="text-align:justify">
					A light-emitting diode (LED) is a semiconductor device or simply a PN junction diode  that emits visible light when an electric current passes through it in forward direction. The amount of light output is directly proportional to the forward current.
					</p>
					
					<p>
					<center><img src="..\images\symbol and connection.jpg" alt="symbol and connection" style="width:500px;height:350px;" align="middle" ></br>
					</center>
					</p>

					<p></p>
					<br>
					<h4><strong>How Light Emitting Diode (LED) works?</strong></h4>
					<p style="text-align:justify">Light Emitting Diode (LED) works only in forward bias condition. When Light Emitting Diode (LED) is forward biased, the free electrons from n-side and the holes from p-side are pushed towards the junction.<br>
					When free electrons reach the junction or depletion region, some of the free electrons recombine with the holes in the positive ions. We know that positive ions have less number of electrons than protons. Therefore, they are ready to accept electrons. Thus, free electrons recombine with holes in the depletion region. In the similar way, holes from p-side recombine with electrons in the depletion region.
					</p>
					<center><img src="..\images\photons_and_electrons.jpg" alt="photons_and_electrons" style="width:600px;height:350px;"></br>
					<p><a href="http://www.physics-and-radio-electronics.com/electronic-devices-and-circuits/semiconductor-diodes/lightemittingdiodeledconstructionworking.html" target="blank">Image source : www.physics-and-radio-electronics.com</a></center>
					</p>
					
					<p style="text-align:justify">Because of the recombination of free electrons and holes in the depletion region, the width of depletion region decreases. As a result, more charge carriers will cross the p-n junction.
					Some of the charge carriers from p-side and n-side will cross the p-n junction before they recombine in the depletion region. For example, some free electrons from n-type semiconductor cross the p-n junction and recombines with holes in p-type semiconductor. In the similar way, holes from p-type semiconductor cross the p-n junction and recombines with free electrons in the n-type semiconductor.
					Thus, recombination takes place in depletion region as well as in p-type and n-type semiconductor. 
					The free electrons in the conduction band releases energy in the form of light before they recombine with holes in the valence band.
					</p>
					
					<br><h4><strong>How LED will be connected to 8051 practically?</strong></h4>
					The below diagram shows the interfacing of a LED with a port pin of microcontroller
					<p></p>
					<center><img src="..\images\interfacing_of_led.jpg" alt="interfacing_of_led" style="width:600px;height:400px;"></br>
					<p>Image source : Designed on CADSOFT EAGLE tool</center>
					</p>
					<p></p>


					<br><h4><strong>Task 2:</strong></h4> Display a digit on seven segment display using 8051 microcontroller.</br>

					<p></p>
					<h4><strong>Theory –</strong></h4>
					<p style="text-align:justify">The 7-segment display consists of seven LEDs arranged in a rectangular fashion. Each of the seven LEDs is called a segment because when illuminated the segment forms part of a numerical digit (both Decimal and Hex) to be displayed. An additional 8th LED is sometimes used within the same package which is the indication of a decimal point(DP), when two or more 7-segment displays are connected together numbers greater than ten can be displayed.<br>
					So by forward biasing the appropriate pins of the LED segments in a particular order, some segments will be glowing and others will remain as it is, allowing the desired character pattern of the number to be generated on the display. This then allows us to display each of the ten decimal digits 0 to 9 on the same 7-segment display.
					</p>
					Now accordingly terminals are taken common, so there are two types of display: <br>
					1. Common Cathode display <br>      
					2. Common Anode display 

					<br>
					 <h4><strong>1. The Common Cathode (CC) –</strong></h4> 
					 <p style="text-align:justify;">In the common cathode display, all the cathode connections of the LED segments are joined together to logic “0” or ground. The individual segments are illuminated by application of a “HIGH”, or logic “1” signal via a current limiting resistor to forward bias the individual Anode terminals (a-g).
					</p>

					<center><img src="..\images\common_cathode.jpg" alt="common_cathode" style="width:500px;height:350px;"></br>
					<p><a href="http://www.electronics-tutorials.ws/blog/7-segment-display-tutorial.html" target="blank">Image source : www.electronics-tutorials.ws</a></p></center>
					<p></p>
					<center><img src="..\images\common_cathode_table.jpg" alt="common_cathode_table" style="width:500px;height:600px;"></br>
					<p></p></center>
					<p></p>

					<br> <h4><strong>2. The Common Anode (CA) –</strong></h4> <p style="text-align:justify;">In the common anode display, all the anode connections of the LED segments are joined together to logic “1”. The individual segments are illuminated by applying a ground, logic “0” or “LOW” signal via a current limiting resistor to the Cathode of the particular segment (a-g).</p>

					<p></p>
					<center><img src="..\images\common_anode.jpg" alt="common_anode" style="width:500px;height:350px;"></br>
					<p><a href="http://www.electronics-tutorials.ws/blog/7-segment-display-tutorial.html"target="blank">Image source : www.electronics-tutorials.ws</a></center>
					</p>
					<p></p>
					<center><img src="..\images\common_anode_table.jpg" alt="common_anode_table" style="width:500px;height:600px;"></center>
					<p></p>

					<br>
					<p style="text-align:justify">In general, common anode displays are more popular as many logic circuits can sink more current than they can source. Also note that a common cathode display is not a direct replacement in a circuit for a common anode display and vice versa, as it is the same as connecting the LEDs in reverse, and hence light emission will not take place.<br>
					Depending upon the decimal digit to be displayed, the particular set of LEDs is forward biased. For instance, to display the numerical digit 0, we will need to light up six of the LED segments corresponding to a, b, c, d, e and f. Then the various digits from 0 through 9 can be displayed using a 7-segment display as shown below.
					</p>

					<center><img src="..\images\all_numbers.jpg" alt="all_numbers" style="width:800px;height:228px;"></br>
					<p><a href="http://www.electronics-tutorials.ws/blog/7-segment-display-tutorial.html" target="blank">Image source : www.electronics-tutorials.ws</a></center>
                    </p>
					<br>

					<strong>Refer the table given below to know the hex values for displaying a specific number on the 7 segment display during the simulation. </strong>
					<p></p>
					<center><img src="..\images\common_cathode_hex_values.bmp" alt="common_cathode_hex_values" style="width:800px;height:300px;"></br>
					<p><a href="http://aimagin.com/blog/how-to-drive-a-7-segment-led/" target="blank">Image source : aimagin.com</a></center>
					</p>

					<!-- theory ends here -->
					<br><br><br><br><br><br>
					<strong>Documentation about 8051 Microcontroller </strong> <br>   <!-- documents related to experiments-->
					<ul>
					<li><a href="..\..\src\pdfs-docs\8051 overview.pdf" target="blank">8051 Overview.pdf</a>   </li> <br>
					<li><a href="..\..\src\pdfs-docs\8051-1.pdf" target="blank">8051 Hardware Overview.pdf</a> </li> <br>
					<li><a href="..\..\src\pdfs-docs\8051IS.pdf" target="blank">8051 Instruction Set.pdf</a>   </li>
					</ul>
					<br>

							
            </p>
        </section>
        <!-- /.content -->
      </div>
      <?php include 'footer.html'; ?>
      <!-- /.content-wrapper -->
        </div>
        
    </body>
	</font>
</html>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>